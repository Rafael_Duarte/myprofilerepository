<?php

require_once('config.php');

$locale = LANG;
$textdomain = "site_multi_lingua";
$locales_dir = dirname(__FILE__) . '/lang';

if (isset($_GET['lang']) && !empty($_GET['lang']))
  $locale = $_GET['lang'];

putenv('LANGUAGE=' . $locale);
putenv('LANG=' . $locale);
putenv('LC_ALL=' . $locale);
putenv('LC_MESSAGES=' . $locale);

require_once('lib/gettext.inc');

_setlocale(LC_ALL, $locale);
_setlocale(LC_CTYPE, $locale);

_bindtextdomain($textdomain, $locales_dir);
_bind_textdomain_codeset($textdomain, 'UTF-8');
_textdomain($textdomain);

function _e($string) {
  echo __($string);
}