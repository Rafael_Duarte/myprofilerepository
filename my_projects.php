<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php include './template/header.php' ?>

<div class="body">

    <div class="row">
        <div class="row my-row">
            <div class="col-md-2">
                <img class="logo" src="img/pocshop-logo.png" alt="pocshop logo">
            </div>
            <div class="col-md-8">
                <a href="http://pocshop.com.br/"><h1 class="profile-card__subtitle">Pocshop</h1></a>
                <p>
                    Mobile application for ecommerce, where the user can buy products and services from local merchants. I work as a software architect, backend developer by providing a mobile API and front end of the system administrative module.
                </p>
                <h2>Technology: PHP, Mysql, Android and swift.</h2> 
            </div>
        </div>
    </div>
</div>





<?php include './template/footer.php' ?>