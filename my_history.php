<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php include './template/header.php' ?>


<div class="body">
     <div class="row my-row">
        <div class="col-md-2" >
            <img class="logo" style="width: 200px;" src="img/ecomp-logo.jpg" alt="university of pernambuco">
        </div>
        <div class="col-md-8">
            <a href="http://www.ecomp.poli.br/" ><h1 class="profile-card__subtitle">MSc in Computer Engineering (2013 - 2015)</h1></a>
            <p>
                My research consists a <a href="https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/trabalhoConclusao/viewTrabalhoConclusao.jsf?popup=true&id_trabalho=2633208#">method</a> 
                and a  <a href="https://youtu.be/2F0OCGFMRr0">tool</a> <a href="https://github.com/duarterafael/Conformitate" rel="nofollow" target="_blank" title="Look through my Github profile">
                    <i class="fa fa-github" aria-hidden="true"></i>
                    
                </a> to evaluate the lack of conformity between the conceptual model 
               (class diagram) and the business process model (BPMN diagram) of an information system.</a>
            </p>
            <a href="http://www.contecsi.fea.usp.br/envio/index.php/contecsi/13CONTECSI/paper/view/4158">
            MENDES, R. G. ; SILVEIRA, D. S. ; DUARTE, R. B. . Um Método para Extração da Carta de Serviços ao 
            Cidadão a partir dos Processos de Negócios. In: 13º CONTECSI - International Conference on Information 
            Systems and Technology Management, 2016, São Paulo. International Conference on Information Systems and 
            Technology Management - CONTECSI, 2016.</a>
            <br>
            <br> 
            <a href="http://ieeexplore.ieee.org/abstract/document/7549295/?section=abstract">
                Duarte, Rafael; SILVA DA SILVEIRA, DENIS ; ARAUJO, JOAO ; WANDERLEY, FERNANDO . 
                Towards a non-conformity detection method between conceptual and business process models. 
                In: 2016 IEEE Tenth International Conference on Research Challenges in Information Science (RCIS), 2016, Grenoble. 2016 IEEE Tenth International Conference on Research Challenges in Information Science (RCIS), 2016. p. 1.</a>
            <br>
            <br>
            <a href="http://capsi.apsi.pt/index.php/capsi/article/view/426/399">
              DUARTE, R. B.; SILVEIRA, D. S. ; WANDERLEY, F. . 
              Um Método para Identificar a Não Conformidade entre os Modelos de Processos de Negócio e o Conceitual de Informação. 
              In: Conferência da Associação Portuguesa de Sistemas de Informação, 2015, Lisboa. 15a Conferência da Associação Portuguesa de Sistemas de Informação - CAPSI, 2015.</a>
            <br>
            <br> 
            <a href="https://drive.google.com/file/d/0B5Rj9wTh25ufN3dpT1JxX1NQUXQzZGtrV1NORW51N3d5M1Nj/view">
             DUARTE, R. B.; SILVEIRA, D. S. . Um Método para Verificar a Conformidade entre os Modelos 
             de Processos de Negócio e o Conceitual de Informação. In: MOSTRA DE EXTENSÃO, INOVAÇÃO E 
             PESQUISA POLI/UPE, 2014, Recife. Mostra de Extensão, Inovação e Pesquisa ? POLI/UPE, 2014.</a>
            <br>
            <br> 
            <a href="http://toc.proceedings.com/27572webtoc.pdf">
            MELO, J. A. S. ; DUARTE, R. B. ; LENCASTRE, M. ; FERNANDES, B. J. T. ; ALENCAR, F. M. R. . 
            Definition of Service - A Systematic Review of the Literature. In: 13th International Conference WWW/Internet, 2014, 
            Porto. IADIS International Conference WWW/Internet, 2014. p. 219-226.</a>

        </div>

    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" src="img/cesar-edu-logo.png" alt="cesar edu logo">
        </div>
        <div class="col-md-8">
            <a href="http://www.cesar.edu.br/"><h1 class="profile-card__subtitle">Postgraduate in Smart Web Solutions and Services (2012 - 2013)</h1></a>
            <p>
                In this the postgraduate course I learned web technologies. Having my course completion work accepted in the 
                <a href="http://www.wikicfp.com/cfp/servlet/event.showcfp?eventid=33604">Workshop on Requirements Engineering</a>.
            </p>
            <a href="http://wer.inf.puc-rio.br/WERpapers/pdf_counter.lua?wer=WER14&file_name=paper36.pdf ">DUARTE, R. B.; JUNIOR, J. ; ARAUJO, R. ; WANDERLEY, F. ; LENCASTRE, M. . 
                Uma Abordagem Colaborativa de Modelagem Conceitual de Informação utilizando Mind Maps. 
                In: WER14 - Workshop em Engenharia de Requisitos, 2014, Pucón. Workshop em Engenharia de Requisitos, 2014.</a>
        </div>

    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" src="img/Bada_Logo.png" alt="samsung bada logo">
        </div>
        <div class="col-md-8">
            <a href="http://anpei.org.br/anpeinews/cesar-e-samsung-lancam-programa-de-residencia-2/"><h1 class="profile-card__subtitle">Bada residence (2010 - 2011)</h1></a>
            <p>
                The Bada residence consists of agreement between the <a href="http://www.cesar.edu.br/">C.E.S.A.R Edu</a> 
                and Samsung with the purpose of popular its virtual store.
            </p>

        </div>
    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" src="img/unicap-logo.jpg" alt="Unicap logo">
        </div>
        <div class="col-md-8">
            <a href="http://www.unicap.br/graduacao/pages/?page_id=88"><h1 class="profile-card__subtitle">UNICAP (2007 - 2011)</h1></a>
            <p>
                I started the course of computer science at the Catholic University of Pernambuco. From the beginning of the graduation I am fascinated by programming, having <a href="https://drive.google.com/file/d/0B2NxN0RtB2fGd1RmV1daajJVY3c/view">final work for the undergratuation</a> in the area of public key cryptography with the objective of digital authentication for mobile devices.
            </p>
        </div>
    </div>

</div>


<?php include './template/footer.php' ?>