<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php include './template/header.php' ?>

<div class="body">
     <div class="row my-row">
        <div class="col-md-2" >
            <img class="logo" src="img/cin-samsung-log.png" alt="cin samsung logo">
        </div>
        <div class="col-md-8">
            <a href="http://bit.blog.br/projeto-samsung-completa-dez-anos-no-cin-da-ufpe-327"><h1 class="profile-card__subtitle">Cin-Samsung project (2016 - Current)</h1></a>
            <p>Development, maintenance and porting of web applications, desktop (Windows) and mobile (Android) for Samsung devices. </p>
            <h2>Technology: C# (Entetity framework, API rest ), SQL Server.</h2> 
        </div>

    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" style="width: 229px;"src="img/stefanini-logo.png" alt="stefanini logo">
        </div>
        <div class="col-md-8">
            <a href="http://documentsolutions.com.br/"><h1 class="profile-card__subtitle">Stefanini - Document Solutions (2015 - 2016)</h1></a>
            <p>Software engineer in web application to automation of business processes using BPMN 2.0 notation.</p>
            <h2>Technology: Java EE, Hibernate,  <a href="https://www.activiti.org/">Activiti</a>, Postregres, Tomcat.</h2> 
        </div>

    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" src="img/tomus-logo.png" alt="tomus logo">
        </div>
        <div class="col-md-8">
            <a href="http://www.tomus.com.br/"><h1 class="profile-card__subtitle">Tomus (2014 - 2015)</h1></a>
            <p>Development of a WEB application for automation of industrial equipment. This tool allowed the monitoring (telemetry) 
                and the sending of commands remotely to an equipment coupled to any machinery of an industry.
            </p>
             <h2>Technology: Java (JSF, Primeface e Hibernate), MySql, Postregres, Tomcat.</h2> 
        </div>
    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" style="width: 140px;" src="img/capes-logo.png" alt="Capes logo">
        </div>
        <div class="col-md-8">
            <a href="http://mestrado.ecomp.poli.br/index.php?option=com_content&task=view&id=36&Itemid=57"><h1 class="profile-card__subtitle">CAPES - Coordenação de Aperfeiçoamento de Pessoal de Nível Superior (2013 - 2014)</h1></a>
            <p>
               CAPES researcher for master's degree development.
            </p>
             <h2>Technology: Java, <a href="https://www.activiti.org/">Activiti</a>, BPMN.20.</h2> 
        </div>
    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" style="width: 200px;" src="img/targetso-logo.png" alt="CESAR logo">
        </div>
        <div class="col-md-8">
            <a href="https://www.targetso.com/servicos-de-ti/"><h1 class="profile-card__subtitle">Target Solutions (2011 - 2011)</h1></a>
            <p>Developer in ERP for large Brazilian distribution companies.</p>
             <h2>Technology: Centura, PL SQL.</h2> 
        </div>
    </div>
    <div class="row my-row">
        <div class="col-md-2">
            <img class="logo" style="width: 200px;" src="img/procenge-logo.png" alt="Procenge logo">
        </div>
        <div class="col-md-8">
            <a href="http://www.procenge.com.br/site/solucoes/softwares/piramide-erp/"><h1 class="profile-card__subtitle">Procenge  (2009 - 2010)</h1></a>
            <p>Developer (intern) of purchase module of ERP pyramid tool.</p>
            <h2>Technology: Centura, PL SQL.</h2> 
        </div>
    </div>
    
</div>
<?php include './template/footer.php' ?>