  <hr>
  <div id="footer">
      <footer>
        
       <div id='icons'>
        <ul class="icons-list">
            <li class="icons-list__item">
                <a href="mailto:rafaelbatistaduarte@gmail.com" rel="nofollow" target="_blank" title="Send me an email">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    Email
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://github.com/duarterafael/" rel="nofollow" target="_blank" title="Look through my Github profile">
                    <i class="fa fa-github" aria-hidden="true"></i>
                    Github
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://www.facebook.com/rafael.duarte.1656" rel="nofollow" target="_blank" title="facebook me!">
                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                    Facebook
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://www.linkedin.com/in/rafael-duarte-21831321/" rel="nofollow" target="_blank" title="See my LinkedIn profile">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                    LinkedIn
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://bitbucket.org/Rafael_Duarte/" rel="nofollow" target="_blank" title="Visit my Bitbucket profile">
                    <i class="fa fa-bitbucket" aria-hidden="true"></i>
                    BitBucket
                </a>
            </li>
            <li class="icons-list__item">
                <a href="skype:rafael batista duarte" rel="nofollow" target="_blank" title="Call me on Skype">
                    <i class="fa fa-skype" aria-hidden="true"></i>
                    Skype
                </a>
            </li>
        </ul>
    </div>
      </footer>
      </div>
    </div > <!-- /container -->        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
