<?php include './template/header.php' ?>

<div class="body">
    <div class="row profile-card__details">
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <img src="img/rafaelduarte.jpg" alt="Rafael Batista Duarte - Software Developer" style="width: 100%;">
        </div> 
        <div class="col-md-6" style="margin-top: 5%;">
            <h1 class="profile-card__title">Rafael Batista Duarte</h1>
            <h2 class="profile-card__subtitle">Software Developer</h2>
        </div>
    </div>
    <hr>
    <div class="home-page__about">
        
        <p Hey, I’m a software developer from in <a href="https://pt.wikipedia.org/wiki/Recife">Recife</a>, Brazil. 
            I'm fascinated by new technologies.  Know a little about  
            <a href="my_history.php" rel="nofollow" target="_blank" title="Know a little about my history">my history</a>. 
           Have a look through some of  <a href="my_projects.php" rel="nofollow" target="_blank" title="Have a look my projects">my projects</a> and 
            <a href="pasts_jobs.php" rel="nofollow" target="_blank" title="and my pasts jobs">pasts jobs</a>.If you like something, please contact me.</p>
    </div>  
        
    <div id='icons'>
        <ul class="icons-list">
            <li class="icons-list__item">
                <a href="mailto:rafaelbatistaduarte@gmail.com" rel="nofollow" target="_blank" title="Send me an email">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    Email
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://github.com/duarterafael/" rel="nofollow" target="_blank" title="Look through my Github profile">
                    <i class="fa fa-github" aria-hidden="true"></i>
                    Github
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://www.facebook.com/rafael.duarte.1656" rel="nofollow" target="_blank" title="facebook me!">
                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                    Facebook
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://www.linkedin.com/in/rafael-duarte-21831321/" rel="nofollow" target="_blank" title="See my LinkedIn profile">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                    LinkedIn
                </a>
            </li>
            <li class="icons-list__item">
                <a href="https://bitbucket.org/Rafael_Duarte/" rel="nofollow" target="_blank" title="Visit my Bitbucket profile">
                    <i class="fa fa-bitbucket" aria-hidden="true"></i>
                    BitBucket
                </a>
            </li>
            <li class="icons-list__item">
                <a href="skype:rafael batista duarte" rel="nofollow" target="_blank" title="Call me on Skype">
                    <i class="fa fa-skype" aria-hidden="true"></i>
                    Skype
                </a>
            </li>
        </ul>
    </div>
        
</div>
    
    
<?php include './template/header.php' ?>